package com.gae.scaffolder.plugin;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import java.util.Map;
import java.util.HashMap;

import android.app.Notification;
import android.app.NotificationChannel;
import android.graphics.Color;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import android.os.Build;
import com.firstmedia.idconnect.R;
import android.support.v4.content.ContextCompat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

/**
 * Created by Felipe Echanique on 08/06/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCMPlugin";
    public static final String PRIMARY_NOTIF_CHANNEL = "default";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        
        // If the application is in the foreground handle both data and notification messages here.
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        Log.d(TAG, "==> MyFirebaseMessagingService onMessageReceived");
		
		/*if( remoteMessage.getNotification() != null){
			Log.d(TAG, "\tNotification Title: " + remoteMessage.getNotification().getTitle());
			Log.d(TAG, "\tNotification Message: " + remoteMessage.getNotification().getBody());
		}*/

        if ( remoteMessage.getNotification() != null) {
            Log.d(TAG, "\tNotification Title: " + remoteMessage.getNotification().getTitle());
            Log.d(TAG, "\tNotification Message: " + remoteMessage.getNotification().getBody());

            Map<String, Object> data = new HashMap<String, Object>();
            data.put("wasTapped", false);
            String title="";
            String pesan="";
            String trackingUrl="";
            for (String key : remoteMessage.getData().keySet()) {
                    Object value = remoteMessage.getData().get(key);
                    Log.d(TAG, "\tKey: " + key + " Value: " + value);


                    data.put(key, value);
                    title = remoteMessage.getData().get("title");
                    pesan = remoteMessage.getData().get("body");
                    trackingUrl = remoteMessage.getData().get("trackingURL");
            }

            sendNotification(remoteMessage.getNotification().getTitle(),remoteMessage.getNotification().getBody(),trackingUrl, data);
        

        }else {
            
        
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("wasTapped", false);
        String title="";
        String pesan="";
        String trackingUrl="";
		for (String key : remoteMessage.getData().keySet()) {
                Object value = remoteMessage.getData().get(key);
                Log.d(TAG, "\tKey: " + key + " Value: " + value);


				data.put(key, value);
                title = remoteMessage.getData().get("title");
                pesan = remoteMessage.getData().get("body");
                trackingUrl = remoteMessage.getData().get("trackingURL");
        }


		
		Log.d(TAG, "\tNotification Data: " + data.toString());
        //FCMPlugin.sendPushPayload( data );
        sendNotification(title,pesan,trackingUrl, data);}
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String title, String messageBody,  String trackingUrl, Map<String, Object> data) {
        

        Intent intent = new Intent(this, FCMPluginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		for (String key : data.keySet()) {
			intent.putExtra(key, data.get(key).toString());
		}

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                //.setSmallIcon(getApplicationInfo().icon)
                .setSmallIcon(R.drawable.notification)
                .setColor(ContextCompat.getColor(this, R.color.colorAccent))
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);


    
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) { //handling oreo foreground
            NotificationChannel chan1 = new NotificationChannel(
                PRIMARY_NOTIF_CHANNEL,
                "default",
                notificationManager.IMPORTANCE_DEFAULT);

                chan1.setDescription(messageBody);
                //chan1.setLightColor(Color.TRANSPARENT);

                notificationManager.createNotificationChannel(chan1);  
                
                notificationBuilder.setChannelId(PRIMARY_NOTIF_CHANNEL);
        }

    
         Notification notification = notificationBuilder.build();

         //notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

         notificationManager.notify(1 /* ID of notification */, notification);

        
    }
}
